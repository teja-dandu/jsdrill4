function pairs(obj) {
    // body...

    // return Object.entries(obj);
    const newArr = [];
    const objKeys = Object.keys(obj);
    const objValues = Object.values(obj);

    if(!obj) return null;
    for(let i = 0; i < objKeys.length; i++){
        newArr.push([objKeys[i], objValues[i]]);
    }
    return newArr;
}

module.exports = pairs;

