function invert(obj) {
    // body...
    const newObj = {};
    const mappedKeys = Object.keys(obj);
    const mappedValues = Object.values(obj);
    if(!obj) return null;
    for(let i = 0; i < mappedKeys.length; i++){
        newObj[mappedValues[i]] = mappedKeys[i];

    }
    return newObj;
}

module.exports = {
    invert,
}

