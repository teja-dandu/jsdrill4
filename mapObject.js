cb = x => "mapped: " + x;

function mapObject(obj, cb) {
    // body...
    //
    if(!obj) return null;
    const mapped = {};
    for(let prop in obj){
        mapped[prop] = cb(obj[prop]);
    }
    return mapped;
}

module.exports = mapObject;

