function defaults(obj, defaultProps) {
    // body...
    Object.keys(defaultProps).forEach((key) => {
        if(Object.prototype.hasOwnProperty.call(obj, key)){
            return;
        }
        obj[key] = defaultProps[key]
    });
    return obj;
}

module.exports = {
    defaults,
};

