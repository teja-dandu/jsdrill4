// const testObjects = {name: 'Bruce Wayne', age: 36, location: 'Gotham'};

let cb = (x) => x;

function keys(obj) {
    // body...
    // return Object.keys(obj);
    let result = [];
    if(!obj || Array.isArray(obj)) return result;
    for(let key of obj){
        result.push(key);
    }
    return result;
}

module.exports = {
    keys,
}

